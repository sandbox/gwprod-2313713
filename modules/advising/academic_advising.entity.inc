<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * The AcademicAdvisingRelationship entity.
 */
class AcademicAdvisingRelationship extends Entity {

  /**
   * Returns the title of this object.
   *
   * @return string
   *   The title of the relationship.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * The default URI of the relationship.
   *
   * @return array
   *   A URI array containing a path.
   */
  protected function defaultUri() {
    return array('path' => 'advising/' . $this->identifier());
  }

}

/**
 * AcademicAdvisingRelationshipController entity controller.
 */
class AcademicAdvisingRelationshipController extends EntityAPIController {

  /**
   * Default Creation Function.
   *
   * @param array $values
   *   An array of passed values.
   *
   * @return mixed
   *   Returns whatever the parent class returns.
   */
  public function create(array $values = array()) {

    $values += array(
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'type' => 'academic_advising_relationship',
    );
    return parent::create($values);
  }

  /**
   * Boilerplate Object builder.
   *
   * @param object $entity
   *   The entity.
   * @param string $view_mode
   *   The view mode.
   * @param string $langcode
   *   The language code.
   * @param array $content
   *   An array of content previously generated.
   *
   * @return array
   *   The renderable array returned by the builder parent.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, array $content = array()) {

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}

/**
 * Implements hook_entity_info().
 */
function academic_advising_relationship_entity_info() {
  $entities = array();
  $entities['academic_advising_relationship'] = array(
    'label' => t('Academic Advising - Relationship'),
    'entity class' => 'AcademicAdvisingRelationship',
    'controller class' => 'AcademicAdvisingRelationshipController',
    'base table' => 'academic_advising_relationship',
    'revision table' => 'academic_advising_relationship_revision',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'relationship_id',
      'bundle' => 'type',
      'revision' => 'revision_id',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(
      'academic_advising_relationship' => array(
        'label' => t('Academic Advising - Relationship'),
        'admin' => array(
          'path' => 'admin/structure/academic_advising_relationship/manage',
          'access arguments' => array('administer academic_advising_relationship entities'),
        ),
      ),
    ),
    'load hook' => 'academic_advising_relationship_load',
    'view modes' => array(
      'full' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
      'list' => array(
        'label' => t('List Item'),
        'custom settings' => TRUE,
      ),
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'academic_advising_relationship_uri',
    'module' => 'academic_advising',
    'access callback' => 'academic_advising_relationship_access',
  );
  return $entities;
}

/**
 * Gets the default URI.
 *
 * @param object $relationship
 *   The relationship object.
 *
 * @return array
 *   Returns an URI array.
 */
function academic_advising_relationship_uri($relationship) {
  return array('path' => 'advising/' . $relationship->identifier());
}

/**
 * Graduation Planner Plan loading function.
 *
 * @param int $relationship_id
 *   The relationship id to search for.
 * @param bool $reset
 *   Reset the static cache.
 *
 * @return object
 *   The relationship object, if found.
 */
function academic_advising_relationship_load($relationship_id, $reset = FALSE) {
  $relationship_ids = (isset($relationship_id) ? array($relationship_id) : array());
  $relationship = academic_advising_relationship_load_multiple($relationship_ids, array(), $reset);
  return $relationship ? reset($relationship) : FALSE;
}

/**
 * Graduation Planner Plan multiple loading function.
 *
 * @param array $relationship_ids
 *   An array of relationship ids.
 * @param array $conditions
 *   An array of conditions to filter by.
 * @param bool $reset
 *   Reset the static cache.
 *
 * @return array
 *   Return an array of entities.
 */
function academic_advising_relationship_load_multiple(array $relationship_ids = array(), array $conditions = array(), $reset = FALSE) {
  return entity_load('academic_advising_relationship', $relationship_ids, $conditions, $reset);
}

/**
 * Returns a title for the graduation relationship.
 *
 * @param object $relationship
 *   The relationship object from the menu router.
 *
 * @return string
 *   A string with the name of the user's graduation relationship.
 */
function academic_advising_relationship_title($relationship) {
  $account = user_load($relationship->uid);
  return format_username($account) . "'s advising";
}